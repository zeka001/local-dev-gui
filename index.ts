import { launch } from "./Backend/src/vendors/Carol.ts";
import { dirname, join } from "./Backend/src/vendors/Path.ts";

import { ConfiguratorBackendContainerStatus } from "./Backend/src/Docker/Status/ConfiguratorBackendContainerStatus.ts";
import { ContentExpressContainerStatus } from "./Backend/src/Docker/Status/ContentExpressContainerStatus.ts";
import { FuryContainerStatus } from "./Backend/src/Docker/Status/FuryContainerStatus.ts";
import { JessiContainerStatus } from "./Backend/src/Docker/Status/JessiContainerStatus.ts";
import { ReverseProxyContainerStatus } from "./Backend/src/Docker/Status/ReverseProxyContainerStatus.ts";
import { ContainerStatusSerialized } from "./Shared/src/ContainerStatusSerialized.ts";
import { startContainer, stopContainer, test } from "./Backend/src/Docker/DockerWrapper.ts";

const folder = join(dirname(new URL(import.meta.url).pathname), "public");
const app = await launch({
    width: 480,
    height: 320
});

const configuratorBackendContainerStatus = new ConfiguratorBackendContainerStatus();
const contentExpressContainerStatus = new ContentExpressContainerStatus();
const furyContainerStatus = new FuryContainerStatus();
const jessiContainerStatus = new JessiContainerStatus();
const reverseProxyContainerStatus = new ReverseProxyContainerStatus();

async function getContainerStatuses(): Promise<ContainerStatusSerialized[]>
{
    return Promise.all([
        configuratorBackendContainerStatus.toJson(),
        contentExpressContainerStatus.toJson(),
        furyContainerStatus.toJson(),
        jessiContainerStatus.toJson(),
        reverseProxyContainerStatus.toJson()
    ]);
}

app.onExit().then(() => Deno.exit(0));
app.serveFolder(folder);

await app.exposeFunction("test", async () => test());

await app.exposeFunction("getContainerStatuses", async () => getContainerStatuses());
await app.exposeFunction("stopContainer", (containerName: string) => stopContainer(containerName));
await app.exposeFunction("startContainer", (containerName: string) => startContainer(containerName));
await app.load("index.html");

// TODO: Check how to bundle all into one executable without dependencies
// https://denotutorials.net/making-desktop-gui-applications-using-deno-webview.html
// https://github.com/webview/webview_deno

// WebSockets Example
// https://github.com/thecodeholic/deno-websocket-chat
