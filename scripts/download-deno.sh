#!/bin/sh

set -e

deno_install="${DENO_INSTALL:-$PWD/.deno}"
bin_dir="$deno_install/bin"
exe="$bin_dir/deno"

if [ -f "$exe" ]; then
	exit 0
fi

if [ "$(uname -m)" != "x86_64" ]; then
	echo "Error: Unsupported architecture $(uname -m). Only x64 binaries are available." 1>&2
	exit 1
fi

if ! command -v unzip >/dev/null; then
	echo "Error: unzip is required to install Deno (see: https://github.com/denoland/deno_install#unzip-is-required)." 1>&2
	exit 1
fi

case $(uname -s) in
Darwin) target="x86_64-apple-darwin" ;;
*) target="x86_64-unknown-linux-gnu" ;;
esac

if [ $# -eq 0 ]; then
	deno_asset_path=$(
		curl -sSf https://github.com/denoland/deno/releases |
			grep -o "/denoland/deno/releases/download/.*/deno-${target}\\.zip" |
			head -n 1
	)
	if [ ! "$deno_asset_path" ]; then
		echo "Error: Unable to find latest Deno release on GitHub." 1>&2
		exit 1
	fi
	deno_uri="https://github.com${deno_asset_path}"
else
	deno_uri="https://github.com/denoland/deno/releases/download/${1}/deno-${target}.zip"
fi

if [ ! -d "$bin_dir" ]; then
	mkdir -p "$bin_dir"
fi

curl --fail --location --progress-bar --output "$exe.zip" "$deno_uri"
cd "$bin_dir"
echo "$PWD"
unzip -o "$exe.zip"
chmod +x "$exe"
rm "$exe.zip"

echo "Deno was downloaded successfully to $exe"
