.PHONY: app dev-be dev-fe download-deno npm-install prod-be prod-fe

DENO = ./.deno/bin/deno
DENOFLAGS =  --unstable --allow-env --allow-read=./,/usr/bin/google-chrome-stable --allow-write=./,/tmp --allow-net=github.com,github-production-release-asset-2e65be.s3.amazonaws.com,127.0.0.1 --allow-plugin --allow-run

# Run locally
app: download-deno npm-install prod-fe prod-be
	${DENO} run ${DENOFLAGS} index.js

# Development
dev-be: download-deno
	${DENO} run ${DENOFLAGS} index.ts
dev-fe: npm-install
	docker-compose run --rm -v ${PWD}:/www -w /www node npm run autobuild

download-deno:
	./scripts/download-deno.sh v1.0.5
npm-install:
	docker-compose run --rm -v ${PWD}:/www -w /www node npm i

# Production
prod-be:
	${DENO} bundle index.ts index.js
prod-fe:
	docker-compose run --rm -v ${PWD}:/www -w /www node npm run build
