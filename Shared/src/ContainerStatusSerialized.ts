export interface ContainerStatusSerialized
{
    name: string;
    isRunning: boolean;
    isCreated: boolean;
    isLoading?: boolean;
}
