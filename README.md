# local-dev-gui

**Over-engineered hybrid application (desktop-browser) to start/stop Kartenmacherei containers.**

A simple script could do the job ... but this one has nice css animations :)

------

Using latest technologies:

- [Deno](https://deno.land/): for the backend, running in your computer
- [Svelte](https://svelte.dev/): for the frontend, running in Chrome
- [Carol](https://github.com/uki00a/carol): glue between backend and frontend, offers direct unidirectional (bidirectional comming) communication between frontend and backend

## Pre-requisites

- git, make, docker
- Google Chrome: Carol runs the application using your locally-installed Google Chrome

## Installation

Clone this repository:

`git clone git@bitbucket.org:zeka001/local-dev-gui.git`

Access the new folder

`cd local-dev-gui`

Start the app

`make`

## Known Issues

- With newer versions of Deno (as of July 2021), Carol gives an error when Deno tries to compile, so for now we are using Deno v1.0.5

- After some time running, the application crashes with `error: Uncaught Error: Too many open files (os error 24)`
