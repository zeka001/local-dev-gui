import { ContainerStatusSerialized } from "../../Shared/src/ContainerStatusSerialized.ts";

export {};

declare global {
    interface window {
        getContainerStatuses(): Promise<ContainerStatusSerialized[]>
        stopContainer(): Promise<boolean>
        startContainer(): Promise<boolean>
    }
}
