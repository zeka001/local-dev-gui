import { ContainerStatusSerialized } from "../../../../Shared/src/ContainerStatusSerialized.ts";

export interface ContainerStatus
{
    isRunning(): Promise<boolean>;
    isCreated(): Promise<boolean>
    toJson(): Promise<ContainerStatusSerialized>;
}
