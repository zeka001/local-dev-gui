import { ContainerStatus } from "./ContainerStatus.ts";
import { ContainerStatusSerialized } from "../../../../Shared/src/ContainerStatusSerialized.ts";
import { getRunningContainersByName, isContainerCreated } from "../DockerWrapper.ts";

export class ContentExpressContainerStatus implements ContainerStatus
{
    private readonly CONTAINER_NAME = 'content-express';
    private readonly EXPECTED_NUMBER_OF_CONTAINERS = 2;

    public async isRunning(): Promise<boolean>
    {
        return (await getRunningContainersByName(this.CONTAINER_NAME)).length === this.EXPECTED_NUMBER_OF_CONTAINERS;
    }

    public async isCreated(): Promise<boolean>
    {
        return (await isContainerCreated(this.CONTAINER_NAME)).length === this.EXPECTED_NUMBER_OF_CONTAINERS;
    }

    public async toJson(): Promise<ContainerStatusSerialized>
    {
        return {
            name: this.CONTAINER_NAME,
            isRunning:  await this.isRunning(),
            isCreated: await this.isCreated(),
        }
    }
}
