export async function test(containerName: string = 'foo'): Promise<string[]>
{
    const result = await Deno.run({
        cmd: ['sh', '-c', `docker ps -a -f name=${containerName} --format "{{.ID}}"`],
        stdout: 'piped'
    });

    const output = await readAndDecodeStout(result);

   return output !== '' ? output.split('\n') : [];
}

export async function getRunningContainersByName(containerName: string): Promise<string[]>
{
    const result = await Deno.run({
        cmd: ['sh', '-c', `docker ps -f name=${containerName} -f status=running --format "{{.ID}}"`],
        stdout: 'piped'
    });

    const output = await readAndDecodeStout(result);

    return output !== '' ? output.split('\n') : [];
}

export async function isContainerCreated(containerName: string): Promise<string[]>
{
    const result = await Deno.run({
        cmd: ['sh', '-c', `docker ps -a -f name=${containerName} --format "{{.ID}}"`],
        stdout: 'piped'
    });

    const output = await readAndDecodeStout(result);

    return output !== '' ? output.split('\n') : [];
}

export async function startContainer(containerName: string): Promise<boolean>
{
    containerName = containerName === 'reverse-proxy' ? 'local-dev-prototype': containerName;

    const result = Deno.run({
        cmd: ['sh', '-c', `cd "\${HOME}"/Kartenmacherei/${containerName} && docker-compose up -d`],
        stdout: 'piped'
    });

    await readAndDecodeStout(result);

    return true;
}

export async function stopContainer(containerName: string): Promise<boolean>
{
    containerName = containerName === 'reverse-proxy' ? 'local-dev-prototype': containerName;

    const result = Deno.run({
        cmd: ['sh', '-c', `cd "\${HOME}"/Kartenmacherei/${containerName} && docker-compose stop`],
        stdout: 'piped'
    });

    await readAndDecodeStout(result);

    return true;
}

async function readAndDecodeStout(result: Deno.Process): Promise<string> {
    if (result.stdout) {
        const buf = new Uint8Array(1024);
        const n = <number>await result.stdout.read(buf);

        return new TextDecoder().decode(buf.subarray(0, n)).trim();
    }

    throw new Error('No result found from stdout');
}
